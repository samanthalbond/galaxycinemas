﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using Common;
using Common.Business_Objects;
using GalaxyCinemas.DataLayer;

namespace GalaxyCinemas
{
    public class MovieImporter : BaseImporter
    {
        //class constructor which passes "filename" parameter to the base class constructor
        public MovieImporter(string filename) : base (filename)
        {
            
        }
        
               
        /// <summary>
        /// Import movie file. Filename has been provided in the constructor.
        /// </summary>
        public override void Import(object o)
        {
            // Initialise progress to zero for progress bar.
            Progress = 0f;
            ImportResult results = new ImportResult();

            try
            {
                // Read file
                string fileData = null;
                using (StreamReader reader = File.OpenText(fileName))
                {
                    // Read file  using ReadToEnd
                    fileData = reader.ReadToEnd();
                }

                string[] lines = fileData.Replace("\r\n", "\n").Replace("\r", "\n").Split('\n'); // To deal with Windows, Mac and Linux line endings the same.
                string firstLine = lines[0];
                string[] columns = firstLine.Split(','); 

                // Check if first line is column names.
                if (columns.Length == 2)
                {
                    string movieid = columns[0].ToLower().Trim(); 
                    string title = columns[1].ToLower().Trim();
                    lines[0] = "";
                }

                // Line count and line numbers to allow progress tracking.
                int lineCount = lines.Length;
                int lineNum = 1;
                
                // Get all movies.               
                List<Movie> movies = DataLayer.DataLayer.GetAllMovies();

                foreach (string line in lines)
                {
                    try 
                    {
                        // Check whether we need to stop after importing each line.
                        if (Stop)
                        {
                            return;
                        }

                        // Just to make it slow enough to test stopping functionality.
                        Thread.Sleep(500);

                        // Update progress of import.
                        Progress = lineNum / lineCount;
                        RaiseProgressChanged();

                        // Skip blank lines
                        if (line.Length == 0)
                            continue;
                        else
                            results.TotalRows++;

                        // Break up line by commas, each item in array will be one column.
                        columns = line.Split(',');
                        if (columns.Length != 2)
                        {
                            results.FailedRows++;
                            results.ErrorMessages.Add(string.Format("Line {0}: Wrong number of columns.", lineNum));
                            continue;
                        }

                        // Check the format of data, and update ImportResult accordingly.
                        int movieID = 0;
                        if (!int.TryParse(columns[0], out movieID))
                        {
                            results.FailedRows++;
                            results.ErrorMessages.Add(string.Format("Line {0}: MovieID is not a number.", lineNum));
                            continue;
                        }

                        string title = columns[1].Trim();
                        if (title.Equals(""))
                        {
                            results.FailedRows++;
                            results.ErrorMessages.Add(string.Format("Line {0}: Title is empty.", lineNum));
                            continue;
                        }                        

                        // Insert/update DB if okay.
                        Movie movieToUpdate = movies.Where(m => m.MovieID == movieID).FirstOrDefault();
                        if (movieToUpdate == null)
                        {
                            Movie movieToAdd = new Movie() { MovieID = movieID, Title = title };
                            DataLayer.DataLayer.AddMovie(movieToAdd);
                        }
                        else
                        {
                            movieToUpdate.Title = title;
                            DataLayer.DataLayer.UpdateMovie(movieToUpdate);
                        }

                        //Increasing number of imported rows 
                        results.ImportedRows++;
                    }
                    finally
                    {
                        lineNum++;
                    }
                }
                }
            catch (IOException)
            {
                results.ErrorMessages.Add(string.Format("Error occurred opening file. Please check that the file exists and that you have permissions to open it."));
            }
            catch (Exception)
            {
                results.ErrorMessages.Add(string.Format("An unknown error occurred during importing."));
            }
            finally
            {
                //Do callback to end import
                RaiseCompleted(results);
            }
            }
    }
}
