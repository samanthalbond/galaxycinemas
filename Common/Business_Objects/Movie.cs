﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Business_Objects
{
    public class Movie
    {
        //public properties to hold information about Movie objects

        public int MovieID { get; set; }
        public String Title { get; set; }
    }
}
